//
//  Builder.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation

protocol Builder {}

extension Builder {
    public func with(configure: (inout Self) -> Void) -> Self {
        var this = self
        configure(&this)
        return this
    }
}
