//
//  ContactPicture.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/6/20.
//

import Foundation

struct ContactPicture {
    var large: String?
    var medium: String?
}
