//
//  ContactName.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/6/20.
//

import Foundation

class ContactName {
    var first: String?
    var last: String?
}
