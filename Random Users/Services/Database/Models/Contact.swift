//
//  UserDb.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/6/20.
//

import Foundation

class Contact {
    var id: Int64?
    var gender: String?
    var country: String?
    var name: ContactName?
    var picture: ContactPicture?
}
