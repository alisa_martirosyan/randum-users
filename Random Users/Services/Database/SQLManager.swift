//
//  SQLManager.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/6/20.
//

import Foundation
import SQLite

class SQLManager {
    
    var db: Connection?
    let usersTable = Table("users")
    var contacts:[Contact] = []
    
    let id = Expression<Int64>("id")
    let firstName = Expression<String?>("fname")
    let lastName = Expression<String?>("lname")
    let country = Expression<String?>("country")
 //   let picture = Expression<String?>("picture")
    let gender = Expression<String?>("gender")
    let databaseFilePath = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\("db.sqlite3")"
    
    init() {
        dbSetup()
//        dbSelectOperation()
    }
    
    func dbSetup(){
        
        db = try! Connection(databaseFilePath)
        
        try! db!.run(usersTable.create(ifNotExists: true) { t in
            t.column(id, primaryKey: true)
            t.column(firstName)
            t.column(lastName)
            t.column(country)
     //       t.column(picture)
            t.column(gender)
        })
    }
    
    func insertData(contact: Contact){
        
        db = try! Connection(databaseFilePath)
        
        do {
            let rowid = try db!.run(usersTable.insert(
                                                      firstName <- contact.name?.first,
                                                      lastName <- contact.name?.last,
                                                      country <- contact.country,
//                                                      picture <- contact.picture?.medium,
                                                      gender <- contact.gender
            ))
            
            print("Row inserted successfully id: \(rowid)")
        }
        catch {
            print("insertion failed: \(error)")
        }
        
    }
    
    //    func removeFromDb(){
    //        do {
    //          let deleteLocation=self.users.filter(self.name == self.temperatureLocationArray[indexPath.row].locationName)
    //
    //          if try self.db!.run(deleteLocation.delete()) > 0 {
    //                  print("deleted successfully")
    //              }
    //                    else {
    //                  print("item for delete not found")
    //                    }
    //          } catch {
    //                print("delete failed: \(error)")
    //        }
    //    }
    //
    
    func dbSelectOperation() -> Int {
        var count = 0
        do {
            for user in try db!.prepare(usersTable) {
                let contact = Contact()
                contact.name?.first = user[firstName]
                contact.name?.last = user[lastName]
                contact.gender = user[gender]
                contact.country = user[country]
        //        contact.picture?.medium = user[picture]
                
                contacts.append(contact)
                count+=1
            }
            
        }catch {
            print ("selection error: \(error)")
            
        }
        return count
    }
}
