//
//  APIManager.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation
import SwiftyJSON

class APIManager {
    
    private class func sendRequest(with url: URL, completionHandler: @escaping (Data?, ResponseError?) -> Void) {
        
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completionHandler(nil, ResponseError(statusCode: (response as? HTTPURLResponse)?.statusCode, message: error.localizedDescription))
                } else if let data = data {
                    completionHandler(data, nil)
                } else {
                    completionHandler(nil, ResponseError(statusCode: (response as? HTTPURLResponse)?.statusCode, message: "unknown error"))
                }
            }
        }
        
        task.resume()
    }
    
    class func fetchUsers(completion: @escaping ((Users?, ResponseError?) -> Void)) {

        let initialUrl: URL? = URL(string: "https://randomuser.me/api")
        
        let finalURL = initialUrl?.appending("results", value:  String(UserDefaults.standard.pageCount))
                                  .appending("gender", value: UserDefaults.standard.gender == 0 ? "female" : "male")
                                  .appending("nat", value: UserDefaults.standard.country)

        guard let url = finalURL else {
            return
        }
        
        sendRequest(with: url) { (data, error) in
            if let error = error {
                completion(nil, error)
            } else if let data = data, let users = Users(json: JSON(data)) {
                completion(users, nil)
            }
        }
    }
}

extension URL {

    func appending(_ queryItem: String, value: String?) -> URL {

        guard var urlComponents = URLComponents(string: absoluteString) else { return absoluteURL }
        var queryItems: [URLQueryItem] = urlComponents.queryItems ??  []
        let queryItem = URLQueryItem(name: queryItem, value: value)
        queryItems.append(queryItem)
        urlComponents.queryItems = queryItems

        return urlComponents.url!
    }
}
