//
//  PictureModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/3/20.
//

import Foundation
import SwiftyJSON

class PictureModel {
    
    var large: String?
    var medium: String?
    
    init?(json: JSON) {
//        print(json)
        
        guard let dict = json.dictionary else {
            return nil
        }
        
        large = dict["large"]?.string
        medium = dict["medium"]?.string
    }
}
