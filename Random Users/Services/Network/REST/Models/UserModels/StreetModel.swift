//
//  StreetModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation
import SwiftyJSON

class StreetModel {
    
    var number: Int?
    var name: String?
    
    init?(json: JSON) {
//        print(json)
        
        guard let dict = json.dictionary else {
            return nil
        }
        
        number = dict["number"]?.int
        name = dict["name"]?.string
    }
}
