//
//  Users.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation
import SwiftyJSON

class UserModel {
    var gender: String?
    var name: NameModel?
    var location: LocationModel?
    var picture: PictureModel?
    var country: String?
    
    init?(json: JSON) {
//        print(json)
        guard let dict = json.dictionary else {
            return nil
        }
        
        gender = dict["gender"]?.string
        country = dict["nat"]?.string
        
        if let nameJson = dict["name"], let name = NameModel(json: nameJson){
            self.name = name
        }
        
        if let locationJson = dict["location"], let location = LocationModel(json: locationJson){
            self.location = location
        }
        
        if let pictureJson =  dict["picture"], let picture = PictureModel(json: pictureJson){
            self.picture = picture
        }
    }
}

class Users {
    
    var items: [UserModel] = []
    
    init() {}
    
    init?(json: JSON) {
    
        guard let array = json.dictionary?["results"]?.array else {
            return nil
        }
    
        for item in array {
            if let user = UserModel(json: item) {
                items.append(user)
            }
        }
    }
    
}

