//
//  ResponseError.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/6/20.
//

import Foundation

struct ResponseError {
    var statusCode: Int?
    var message: String
}
