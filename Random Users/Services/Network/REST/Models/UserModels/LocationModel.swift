//
//  LocationModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation
import SwiftyJSON

class LocationModel {
    
    var street: StreetModel?
    var city: String?

    init?(json: JSON) {

        guard let dict = json.dictionary else {
            return nil
        }

        if let streetJSON = dict["street"], let street = StreetModel(json: streetJSON) {
            self.street = street
        }
    }
}
