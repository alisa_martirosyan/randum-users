//
//  NameModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import SwiftyJSON

class NameModel {
    
    var title: String?
    var first: String?
    var last: String?
    
    init?(json: JSON) {
//        print(json)
        
        guard let dict = json.dictionary else {
            return nil
        }
        
        title = dict["title"]?.string
        first = dict["first"]?.string
        last = dict["last"]?.string
    }
}
