//
//  UserDefaults.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/5/20.
//

import Foundation

extension UserDefaults {
    
    @objc var pageCount: Int {
        get {
            return integer(forKey: "page")
        }
        set {
            set(newValue, forKey: "page")
        }
    }
    
    @objc var gender: Int {
        get {
            return integer(forKey: "gender")
        }
        set {
            set(newValue, forKey: "gender")
        }
    }
    
    @objc var country: String {
        get {
            return string(forKey: "country") ?? ""
        }
        set {
            set(newValue, forKey: "country")
        }
    }
    
    @objc var countryName: String {
        get {
            return string(forKey: "countryName") ?? ""
        }
        set {
            set(newValue, forKey: "countryName")
        }
    }
}
