//
//  PageSizeCell.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 11/13/20.
//

import UIKit

class PageSizeCell: UITableViewCell {
    @IBOutlet weak var pageSlider: UISlider!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onChangePageSizeSliderValue(_ sender: UISlider!) {
        DispatchQueue.main.async {
            UserDefaults.standard.pageCount = Int(sender.value)
        }
    }

}
