//
//  GenderCell.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 11/13/20.
//

import UIKit

class GenderCell: UITableViewCell {
    @IBOutlet weak var genderSegmentedControl: UISegmentedControl!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func onChangeGenderSegmentedValue(_ sender: UISegmentedControl!) {
        DispatchQueue.main.async {
            UserDefaults.standard.gender = sender.selectedSegmentIndex
        }
    }
    
}
