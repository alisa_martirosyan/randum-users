//
//  Settings.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/5/20.
//

import Foundation

class Settings: NSObject {
    var pages: Int = 0
    var gender: Int = 0
    var nationality: String = ""
    var countryName: String = ""

    init(pages: Int, gender: Int, nationality: String, countryName: String) {
        self.pages = pages
        self.gender = gender
        self.nationality = nationality
        self.countryName = countryName
    }
}
