//
//  SettingsViewModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/1/20.
//

import Foundation
import Combine

class SettingsViewModel {
    
    let objectWillChange = PassthroughSubject<Void, Never>()
    let defaults = UserDefaults.standard
    @Published private (set) var settings:Settings = Settings(pages: 20, gender: 0, nationality: "AL", countryName: "")
    
    func getSettings(){
        let gender = UserDefaults.standard.gender
        let pages = UserDefaults.standard.pageCount
        let country = UserDefaults.standard.country
        let countryName = UserDefaults.standard.countryName
        settings = Settings(pages: pages, gender: gender, nationality: country, countryName: countryName)
    }
    
    func saveGenderState(genderIndex: Int) {
        UserDefaults.standard.gender = genderIndex
    }
    
    func savePageCount(pageCount: Int) {
        UserDefaults.standard.pageCount = pageCount
    }
    
    func saveCountryState(nationality: String) {
        UserDefaults.standard.country = nationality
    }
    
    func saveCountryNameState(countryName: String) {
        UserDefaults.standard.countryName = countryName
    }
}
