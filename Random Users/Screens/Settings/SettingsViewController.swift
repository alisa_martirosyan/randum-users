//
//  SettingsScreen.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/1/20.
//

import UIKit
import CountryPickerView
import Combine

class SettingsViewController: UIViewController {
    
    private var viewModel: SettingsViewModel = SettingsViewModel()
    private var cancellables: Set<AnyCancellable> = []
    var tableView:UITableView = UITableView(frame: .zero, style: .grouped)
    let countryPickerView = CountryPickerView()
    
    var pageSizeCell:PageSizeCell?
    var genderCell:GenderCell?
    var countryCell:CountryCell?
    
    var sliderValue: Float = 20
    var countryName: String = ""
    var segmentedControlIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Settings"
        
        initCountryPicker()
        viewModel.getSettings()
        bindViewModel()
        initTableView()
    }
    
    private func bindViewModel() {
        viewModel.$settings.sink { [weak self] settings in
            self?.renderSettings(settings: settings)
        }.store(in: &cancellables)
    }
    
    private func renderSettings(settings: Settings){
        sliderValue = Float(settings.pages)
        segmentedControlIndex = settings.gender
        countryName = settings.countryName
    }
    
    func initCountryPicker() {
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
    }
}

extension SettingsViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return 1
        default:
            return 2
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Response Page Size"
        default:
            return "User Filters"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if indexPath.row == 1 && indexPath.section == 1{
            if let nav = self.navigationController {
                countryPickerView.showCountriesList(from: nav)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        self.pageSizeCell = tableView.dequeueReusableCell(withIdentifier: "PageSizeCell") as? PageSizeCell
        genderCell = tableView.dequeueReusableCell(withIdentifier: "GenderCell") as? GenderCell
        countryCell = tableView.dequeueReusableCell(withIdentifier: "CountryCell") as? CountryCell
        
        countryCell?.detailTextLabel?.text = countryName
        countryCell?.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
        pageSizeCell?.pageSlider.value = sliderValue
        genderCell?.genderSegmentedControl.selectedSegmentIndex = segmentedControlIndex

        switch indexPath.section {
        case 0:
            return pageSizeCell!
        default:
            switch indexPath.row {
            case 0:
                return genderCell!
            default:
                return countryCell!
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
}

extension SettingsViewController: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        DispatchQueue.main.async {
            self.viewModel.saveCountryState(nationality: country.code)
            self.viewModel.saveCountryNameState(countryName: country.name)
        }
        countryCell?.detailTextLabel?.text = country.name
        return
    }
}

extension SettingsViewController {
    
    func initTableView() {
        
        let nibPageSize = UINib(nibName: "PageSizeCell", bundle: nil)
        let nibGender = UINib(nibName: "GenderCell", bundle: nil)
        let nibCountry = UINib(nibName: "CountryCell", bundle: nil)
        
        tableView.register(nibPageSize, forCellReuseIdentifier: "PageSizeCell")
        tableView.register(nibGender, forCellReuseIdentifier: "GenderCell")
        tableView.register(nibCountry, forCellReuseIdentifier: "CountryCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo:view.safeAreaLayoutGuide.rightAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}
