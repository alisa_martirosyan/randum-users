//
//  FavoritesViewController.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation
import UIKit
import Combine

class FavoritesViewController: UIViewController {
    var viewModel: FavoritesViewModel = FavoritesViewModel()
    var usersDataSource: UsersDataSource?
    private var selectedIndexPath: IndexPath? = nil
    private var cancellables: Set<AnyCancellable> = []
    
    var tableView = UITableView(frame: .zero).with {
        let nib = UINib(nibName: "UserInfoTableViewCell", bundle: nil)
        $0.register(nib, forCellReuseIdentifier: "UserInfoTableViewCell")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        setUpTableView()
    }

    private func setUpTableView() {
        view.addSubview(tableView)

        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
}

// MARK: - DataSource
//class UsersDataSource: TableArrayDataSource<UserData, UserInfoTableViewCell> {}

extension FavoritesViewController {
    private func setUpDataSource(_ users:[UserModel]) -> UsersDataSource? {
        
        var models:[UserData] = []
        for i in users {
            models.append(UserData(firstname: (i.name?.first)!, lastname: (i.name?.last)!, image: (i.picture?.medium)!, isFavorite: false))
        }
        
        let dataSource = UsersDataSource(tableView: tableView, array: models)
        dataSource.tableRowSelectionHandler = { [weak self] indexPath in
            guard let strongSelf = self else {
                return
            }
            strongSelf.selectedIndexPath = indexPath
        }
        return dataSource
    }
}
