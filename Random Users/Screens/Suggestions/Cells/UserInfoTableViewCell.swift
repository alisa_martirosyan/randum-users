//
//  UserInfoCellTableViewCell.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell, ConfigurableCell {
    
    @IBOutlet weak var name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - ConfigurableCell
    func configure(_ item: UserData, at indexPath: IndexPath) {
        name.text = item.firstname + " " + item.lastname
        
//        lastNameLabel.text = item.lastname
        return
    }
}

//extension UIImageView {
//    func load(url: URL) {
//        DispatchQueue.global().async {
//            let data = try? Data(contentsOf: url)
//            DispatchQueue.main.async {
//                self.image = UIImage(data: data!)
//            }
//        }
//    }
////
//    func circle(){
//        layer.borderWidth = 1
//        layer.masksToBounds = false
//        layer.borderColor = UIColor.black.cgColor
//        layer.cornerRadius = self.frame.height / 2
//        clipsToBounds = true
//    }
//}
