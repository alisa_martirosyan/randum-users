//
//  UserCellViewModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation

struct UserData:Favorable {
    var favorite: Bool
    
    var firstname: String
    var lastname: String
    var image: String
//    var isFavorite: Bool
    
    public init(firstname: String,lastname: String, image: String, isFavorite:Bool) {
        self.firstname = firstname
        self.lastname = lastname
        self.image = image
        self.favorite = isFavorite
//        self.isFavorite = isFavorite
    }
}
