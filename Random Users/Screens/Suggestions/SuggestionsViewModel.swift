//
//  SuggestionsViewModel.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/1/20.
//

import Foundation

final class SuggestionsViewModel {
    private var apiManager: APIManager!
    private var sqlManager: SQLManager!
    let users = Box<Users?>(nil)
    let contacts = Box<[Contact]?>([])
    let showProgressIndicator = Box(false)
    let showError = Box(false)
    
    init() {
        self.apiManager = APIManager()
        self.sqlManager = SQLManager()
        generateUsers()
     //   reloadDataFromDb()
    }
    
    func generateUsers(){
        showProgressIndicator.value = true
        APIManager.fetchUsers() { [self] result, error  in
            showProgressIndicator.value = false
            
            if let users = result {
                self.users.value = users
            }else{
                showError.value = true
            }
            
//            DispatchQueue.main.async{
//                saveUsersInDb()
//            }
        }
    }
    
    func reloadDataFromDb(){
        let rowCount = sqlManager.dbSelectOperation()
        if(rowCount>0){
            print ("data available in the database")
        } else{
            generateUsers()
            print ("database is empty")
        }
    }
//
//    func saveUsersInDb() {
//        if let users = self.users.value {
//            for i in 0...users.items.count{
//                let contact = Contact()
//                contact.name?.first = users.items[i].name?.first
//                contact.name?.last =  users.items[i].name?.last
//                contact.gender = users.items[i].gender
//                contact.country = users.items[i].country
//
//                self.sqlManager.insertData(contact:contact)
//            }
//        }
//    }
    
    
}
