//
//  SuggestionsViewController.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import UIKit
import Combine

class SuggestionsViewController: UIViewController {
    private var viewModel : SuggestionsViewModel = SuggestionsViewModel()
    //  private var selectedIndexPath: IndexPath? = nil
    var usersDataSource: UsersDataSource?
    var indicator: UIActivityIndicatorView?
    
    var tableView = UITableView(frame: .zero).with {
        let nib = UINib(nibName: "UserInfoTableViewCell", bundle: nil)
        $0.register(nib, forCellReuseIdentifier: "UserInfoTableViewCell")
    }
    
    let refreshControl = UIRefreshControl().with {
        $0.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator = UIActivityIndicatorView.customIndicator(at: self.view.center)
        
        setUpTableView()
        bindViewModel()
    }
    
    @objc func refresh(sender: UIRefreshControl) {
        viewModel.generateUsers()
        tableView.reloadData()
        sender.endRefreshing()
    }
    
    private func setUpTableView(){
        tableView.refreshControl = refreshControl
        tableView.addSubview(indicator!)
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo:view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo:view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }
    
    private func bindViewModel(){
        viewModel.showProgressIndicator.bind { [weak self] showProgressIndicator in
            self?.progressAnimation(show: showProgressIndicator)
        }
        
        viewModel.users.bind { [weak self] users in
            self?.usersDataSource = self?.setUpDataSource(users?.items ?? [])
            self?.tableView.reloadData()
        }
        
        viewModel.showError.bind {[weak self] showError in
            self?.errorAlert(show: showError)
        }
    }
    
    private func errorAlert(show: Bool){
        let alert = UIAlertController(title: "Network Error", message: "Could not get users list. Please try later", preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func progressAnimation(show: Bool){
        if show {
            indicator?.startAnimating()
        } else {
            indicator?.stopAnimating()
            indicator?.removeFromSuperview()
        }
    }
    
}

// MARK: - DataSource
class UsersDataSource: TableArrayDataSource<UserData, UserInfoTableViewCell> {}

// MARK: - Private Methods
extension SuggestionsViewController {
    private func setUpDataSource(_ users:[UserModel]) -> UsersDataSource? {
        
        var models:[UserData] = []
        for i in users {
            models.append(UserData(firstname: (i.name?.first)!, lastname: (i.name?.last)!, image: (i.picture?.medium)!, isFavorite: false))
        }
        
        let dataSource = UsersDataSource(tableView: tableView, array: models)
        //        dataSource.tableRowSelectionHandler = { [weak self] indexPath in
        //            guard let strongSelf = self else {
        //                return
        //            }
        //            strongSelf.selectedIndexPath = indexPath
        //        }
        return dataSource
    }
}
