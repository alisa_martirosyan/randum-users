//
//  CircleUIImageView.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/3/20.
//

import UIKit

class CircleUIImageView: UIImageView {
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderWidth = 1
        layer.masksToBounds = false
        layer.borderColor = UIColor.black.cgColor
        layer.cornerRadius = self.frame.height / 2
        clipsToBounds = true
    }
}
