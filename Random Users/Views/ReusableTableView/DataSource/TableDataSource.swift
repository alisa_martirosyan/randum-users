//
//  TableDataSource.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import UIKit

protocol Favorable {
    var favorite: Bool { get set }
}

public typealias TableRowSelectionHandlerType = (IndexPath) -> Void

class TableDataSource<Provider: TableDataProvider, Cell: UITableViewCell>:
    NSObject,
    UITableViewDataSource,
    UITableViewDelegate
where Provider.T:Favorable, Cell: ConfigurableCell, Provider.T == Cell.T {
    
    // MARK: - Delegates
    public var tableRowSelectionHandler: TableRowSelectionHandlerType?
    
    // MARK: - Private Properties
    let provider: Provider
    var tableView: UITableView
    
    // MARK: - Lifecycle
    init(tableView: UITableView, provider: Provider) {
        self.tableView = tableView
        self.provider = provider
        super.init()
        setUp()
    }
    
    private func setUp() {
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    // MARK: - UITableViewDataSource
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        provider.numberOfItems(in: section)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Cell.reuseIdentifier,
                                                       for: indexPath) as? Cell else {
            return UITableViewCell()
        }
        cell.accessoryType = .disclosureIndicator
        
        let row = provider.row(at: indexPath)
        if let row = row {
            cell.configure(row, at: indexPath)
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableRowSelectionHandler?(indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
        
    public func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        guard let favorite = provider.row(at: indexPath)?.favorite else {
          return nil
        }

        let title = favorite ?
          NSLocalizedString("Unfavorite", comment: "Unfavorite") :
          NSLocalizedString("Favorite", comment: "Favorite")

        let action = UIContextualAction(style: .normal, title: title,
          handler: { (action, view, completionHandler) in

            self.provider.updateItem(at: indexPath ,isFavorite:!favorite)
            completionHandler(true)
        })

        action.image = favorite ? UIImage(systemName: "heart.fill") : UIImage(systemName: "heart")
        let configuration = UISwipeActionsConfiguration(actions: [action])
        return configuration
    }
}
