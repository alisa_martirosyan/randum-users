//
//  TableArrayDataSource.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import UIKit

class TableArrayDataSource<T:Favorable, Cell: UITableViewCell>: TableDataSource<ArrayDataProvider<T>, Cell>
where Cell: ConfigurableCell, Cell.T == T {
    
    // MARK: - Lifecycle
    public init(tableView: UITableView, array: [T]) {
        let provider = ArrayDataProvider(array: array)
        super.init(tableView: tableView, provider: provider)
    }
    
    // MARK: - Public Methods
    public func row(at indexPath: IndexPath) -> T? {
        return provider.row(at: indexPath)
    }
    
    public func updateItem(at indexPath: IndexPath, isFavorite: Bool) {
        provider.updateItem(at: indexPath, isFavorite: isFavorite)
    }
}

