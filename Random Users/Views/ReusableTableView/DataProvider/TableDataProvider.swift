//
//  TableDataProvider.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation

public protocol TableDataProvider {
    associatedtype T

    func numberOfItems(in section: Int) -> Int
    func row(at indexPath: IndexPath) -> T?
    func updateItem(at indexPath: IndexPath, isFavorite: Bool)
}
