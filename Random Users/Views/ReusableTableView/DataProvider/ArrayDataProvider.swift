//
//  ArrayDataProvider.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation
class ArrayDataProvider<T:Favorable>: TableDataProvider {
    
    // MARK: - Internal Properties
    var rows: [T] = []
    
    // MARK: - Lifecycle
    init(array: [T]) {
        rows = array
    }
    
    // MARK: - TableDataProvider
    public func numberOfItems(in section: Int) -> Int {
        return rows.count
    }
    
    public func row(at indexPath: IndexPath) -> T? {
        guard indexPath.row >= 0 && indexPath.row < rows.count else {
            return nil
        }
        return rows[indexPath.row]
    }
    
    public func updateItem(at indexPath: IndexPath,isFavorite: Bool) {
        guard indexPath.row >= 0 && indexPath.row < rows.count else{
            return
        }
        rows[indexPath.row].favorite = isFavorite
//        rows[indexPath.row] = value
    }
}
