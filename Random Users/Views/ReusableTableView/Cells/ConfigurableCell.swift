//
//  ConfigurableCell.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation

public protocol ConfigurableCell: ReusableCell {
    associatedtype T
    func configure(_ item: T, at indexPath: IndexPath)
}
