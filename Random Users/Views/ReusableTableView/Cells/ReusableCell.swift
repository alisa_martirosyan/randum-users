//
//  ReusableCell.swift
//  Random Users
//
//  Created by Alisa Martirosyan on 10/2/20.
//

import Foundation

public protocol ReusableCell {
    static var reuseIdentifier: String { get }
}

public extension ReusableCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
